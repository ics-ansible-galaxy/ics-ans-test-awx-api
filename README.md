ics-ans-test-awx-api
====================

This playbook is just a test playground.
It was created to test the AWX API from CSEntry.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
